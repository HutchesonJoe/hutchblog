#! /bin/bash
# Directory containing the markdown files
directory="/home/seaneon/git/blog/"
# Get today's date in the format YYYY-MM-DD
today_date=$(date +%Y-%m-%d)
# Iterate over each markdown file in the directory
for file in "$directory"/*.md; do
    # Extract the base filename without the path and .md extension
    filename=$(basename -- "$file" .md)
    
    # Replace dashes with spaces for the title
    title=${filename//[-]/ }
    # Prepare the front matter
    front_matter="---
title: \"$title\"
date: $today_date
draft: false
---"
    # Read the original content of the file
    original_content=$(cat "$file")
    # Write the front matter followed by the original content back to the file
    echo -e "$front_matter\n$original_content" > "$file"
done
echo "Front matter added to all markdown files."
