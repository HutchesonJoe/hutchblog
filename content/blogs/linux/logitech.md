---
title: "Logitech Settings in Linux"
date: 2024-3-7
draft: false
headingPost: "Author: Stu Feeser"
---

![image](/images/v4l.png)

If you are running a logitech webcam on a linux PC, you may not find a GUI based product to control your camera. Not to worry. The "v4l-utils" package is a collection of utilities for Video4Linux (V4L) and DVB users. It provides a series of tools and libraries designed to help users manipulate and interact with video capture and broadcast capabilities on Linux systems. The history and development of v4l-utils are closely tied to the evolution of the Video4Linux system itself, which is a part of the Linux kernel that provides a consistent interface for handling video devices.

1. Install Video for Linux (v4l).

    `$` `sudo apt install v4l-utils`


0. List the video devices hosted on your linux system.

    `$` `v4l2-ctl --list-devices`

    ```bash
    UVC Camera (046d:0990) (usb-0000:00:14.0-4):
            /dev/video0
            /dev/video1
            /dev/media0
    ```


0. Use the v4l2-ctl tool to list controls available from your webcam.

    `$` `v4l2-ctl -d /dev/video0 --list-ctrls`

    ```bash
    User Controls

                         brightness 0x00980900 (int)    : min=0 max=255 step=1 default=128 value=128
                           contrast 0x00980901 (int)    : min=0 max=255 step=1 default=32 value=32
                         saturation 0x00980902 (int)    : min=0 max=255 step=1 default=32 value=32
            white_balance_automatic 0x0098090c (bool)   : default=1 value=1
                               gain 0x00980913 (int)    : min=0 max=255 step=1 default=0 value=198
               power_line_frequency 0x00980918 (menu)   : min=0 max=2 default=2 value=2 (60 Hz)
          white_balance_temperature 0x0098091a (int)    : min=0 max=10000 step=10 default=4000 value=4645 flags=inactive
                          sharpness 0x0098091b (int)    : min=0 max=255 step=1 default=224 value=224
             backlight_compensation 0x0098091c (int)    : min=0 max=2 step=1 default=1 value=1

    Camera Controls

                      auto_exposure 0x009a0901 (menu)   : min=0 max=3 default=3 value=3 (Aperture Priority Mode)
             exposure_time_absolute 0x009a0902 (int)    : min=1 max=10000 step=1 default=166 value=155 flags=inactive
         exposure_dynamic_framerate 0x009a0903 (bool)   : default=0 value=1
    ```

    > v4l-utils includes tools such as v4l2-ctl for querying and setting device attributes, libv4l for easy access to V4L devices through a library of convenience functions, and qv4l2 which is a Qt-based GUI application that provides a user interface for V4L2 device control.


0. Display the current value of one of those settings

    `$` `v4l2-ctl -d /dev/video0 --get-ctrl=brightness`

    ```bash
    brightness: 128
    ```

0. Now change that value, using the set command (replace "get" with "set")

    `$` `v4l2-ctl -d /dev/video0 --set-ctrl=brightness=129`

0. Notice that the current settings are availble for all configurable parameters:

    `$` `v4l2-ctl -d /dev/video0 --list-ctrls`

    ```
    User Controls

                         brightness 0x00980900 (int)    : min=0 max=255 step=1 default=128 value=129 <-- THERE IT IS!
                           contrast 0x00980901 (int)    : min=0 max=255 step=1 default=32 value=32
                         saturation 0x00980902 (int)    : min=0 max=255 step=1 default=32 value=32
            white_balance_automatic 0x0098090c (bool)   : default=1 value=1
                               gain 0x00980913 (int)    : min=0 max=255 step=1 default=0 value=198
               power_line_frequency 0x00980918 (menu)   : min=0 max=2 default=2 value=2 (60 Hz)
          white_balance_temperature 0x0098091a (int)    : min=0 max=10000 step=10 default=4000 value=4645 flags=inactive
                          sharpness 0x0098091b (int)    : min=0 max=255 step=1 default=224 value=224
             backlight_compensation 0x0098091c (int)    : min=0 max=2 step=1 default=1 value=1

    Camera Controls

                      auto_exposure 0x009a0901 (menu)   : min=0 max=3 default=3 value=3 (Aperture Priority Mode)
             exposure_time_absolute 0x009a0902 (int)    : min=1 max=10000 step=1 default=166 value=155 flags=inactive
         exposure_dynamic_framerate 0x009a0903 (bool)   : default=0 value=1
    ```

0. If you reboot your PC, all the changes you made will be lost, and this is where udev and device node files come into play.  Here are the definitions:
    - `udev`  The Linux kernel device manager that stores peripheral device status in ***device node files*** which are stored in the /dev directory. 
    - `node files` Device details that contain massive informaion on each device
    - `udevadm` A tool used to read node files which in turn can be used to select, track events, and test  specific devices
    - `udev rules` These are configuration files that can ID devices, name devices, handle device permissions, run commands on specific devices, set environmental variables, and perform device configuraiton. This last item is what interests us

0. In order to write a udev rule we need to read the device node file for /dev/video0. (Remember, this may be different in your system.) These four criteria will effectively select ONLY our logitech camera and nothing else. Here are the udev keys we will need:
    - `SUBSYSTEM` - The supported subsystem must be ***video4linux***
    - `KERNEL` - Any video device[0-9]
    - `ATTRS{product}` - The product attribute
    - `ATTRS{serial}` - The serial number

0. Gather SUBSYSTEM items from the /dev/video0 node file as follows:

    `$` `udevadm info --attribute-walk --name=/dev/video0 | grep SUBSYSTEM`

    ```bash
    SUBSYSTEM=="video4linux"
    SUBSYSTEMS=="usb"
    SUBSYSTEMS=="usb"
    SUBSYSTEMS=="usb"
    SUBSYSTEMS=="pci"
    SUBSYSTEMS==""
    ```
    
    > the `SUBSYSTEM=="video4linux"` is what we want here, lets move on.

0. Gather the KERNEL data next

    `$` `udevadm info --attribute-walk --name=/dev/video0 | grep KERNEL`

    ```bash
    KERNEL=="video0"
    KERNELS=="3-4:1.0"
    KERNELS=="3-4"
    KERNELS=="usb3"
    KERNELS=="0000:00:14.0"
    KERNELS=="pci0000:00"
    ```

    > `KERNEL=="video0"` will work here, but we can wildcard the 0 value regex-style to be be `[0-9]*`

0. Gather the product info next.

    `$` `udevadm info --attribute-walk --name=/dev/video0 | grep product`

    ```bash
    ATTRS{product}=="xHCI Host Controller"
    ```
    > `ATTRS{product}=="xHCI Host Controller"` is the only one, so this is a sure thing.


0. Finally, gather the serial number, again remember that your number will be different that mine so don't just copy my example!

    `$` `udevadm info --attribute-walk --name=/dev/video0 | grep serial`

    ```bash
    ATTRS{serial}=="67540226"
    ATTRS{serial}=="0000:00:14.0"
    ```
    > `ATTRS{serial}=="67540226"` will work just fine.

0. Now use all that selection citeria to select the logitech camera and persist persist the brightness change as follows. 

    `$` `sudo vim /etc/udev/ruless.d/99-logitech.rules`

    ```
    # Logitech camera persistant settings
    SUBSYSTEM=="video4linux", KERNEL=="video[0-9]*, ATTRS{product}=="xHCI Host Controller", ATTRS{serial}=="67540226", RUN="/usr/bin/v4l2-ctl -d $devnode --set-ctrl=brightness=128"
    ```
    > Study the above command. Notice that all the selection criteria are comma delimited in the above command. Cleary the RUN command will only run on the device that meets all the supplied criteria.

0. If you have a wavy or flickering screen, most likely your country's power source is 50Hz, not 60Hz, so modify the following udev rule to fix that:


    `$` `sudo vim /etc/udev/ruless.d/99-logitech.rules`

    ```bash
    # Logitech camera persistant 50Hz power line settings
    SUBSYSTEM=="video4linux", KERNEL=="video[0-9]*, ATTRS{product}=="xHCI Host Controller", RUN="/usr/bin/v4l2-ctl -d $devnode --set-ctrl=power_line_frequency=1"
    ```


