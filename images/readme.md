Given your setup and requirements, here's a step-by-step guide to store all your blog images in the `alta3/blog` repository on GitLab using Git Large File Storage (LFS) from Ubuntu 22.04 on Windows Subsystem for Linux (WSL):

### Step 1: Install Git LFS

1. Open your terminal in WSL.
2. First, ensure your package lists are up to date:

   ```bash
   sudo apt update
   ```

3. Install Git LFS:

   ```bash
   sudo apt install git-lfs
   ```

### Step 2: Configure Git LFS for Your Repository

1. Navigate to your local clone of the repository:

   ```bash
   cd path/to/alta3/blog
   ```

   Replace `path/to/alta3/blog` with the actual path to your local clone of the repository.

2. Set up Git LFS in your repository:

   ```bash
   git lfs install
   ```

3. Track image files with Git LFS. Assuming your images are in common formats (e.g., JPG, PNG), run:

   ```bash
   git lfs track "*.png"
   git lfs track "*.jpg"
   git lfs track "*.gif"
   ```

   This command tells Git LFS to manage files matching these patterns. You can adjust the file patterns according to the types of images you have.

4. Verify that the `.gitattributes` file is created or updated correctly:

   ```bash
   cat .gitattributes
   ```

   This step is to ensure that the image file patterns are correctly added to the `.gitattributes` file, which should now include lines like:

   ```
   *.png filter=lfs diff=lfs merge=lfs -text
   *.jpg filter=lfs diff=lfs merge=lfs -text
   *.gif filter=lfs diff=lfs merge=lfs -text
   ```

### Step 3: Add Your Images to Git LFS

1. Copy all your images to the `/images` directory to the staging area and then run this:

   ```bash
   git add images/
   ```

2. Commit the changes:

   ```bash
   git commit -m "Add blog images using Git LFS"
   ```

### Step 4: Push Your Changes to GitLab

1. Push your changes to GitLab:

   ```bash
   git push origin main
   ```

   Replace `main` with the name of your branch if it's different.

### Additional Tips

- **Check LFS Storage Quota**: GitLab provides a limited amount of LFS storage for free. Make sure your usage fits within your plan's limits.
- **LFS Migration**: If you've already committed images to your repository without LFS and want to migrate those files to LFS, you can use the `git lfs migrate` command. Refer to the Git LFS documentation for detailed instructions.
- **Troubleshooting**: If you encounter any issues with pushing files or LFS quota limits, check the repository settings on GitLab or consider upgrading your GitLab plan for more LFS storage if necessary.

Following these steps, all new images added to the `/images` directory in your `alta3/blog` repository will be managed by Git LFS, optimizing your repository's size and handling of large files.
