---
title: "Installing Windows Subsystem For Linux 2 Wsl2"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---

  {{< youtube -cRxoI87BXM >}}


Windows Subsystem for Linux 2 (WSL2) is a fast and efficient development platform that blows away WSL1. The older WSL1 is painfully slow for certain tasks.  With WSL2 installed, you'll have a complete ubuntu 20.04 system running on your Windows 10 machine, which performs like you are directly connected to a bare metal ubuntu server. We'll use WSL2 for running other demonstrations in this series.

### Objectives:
  1. Install WSL2
  2. Why WSL1 is not preferred

### Tasks:
1. Make sure you are running the lastest Windows 10. Use the **cmd.exe** executable by pressing your Windows key and then typing in cmd to open up your cmd terminal. Then run `ver` inside the cmd terminal. You should see the following version or later:

    ```
    Microsoft Windows [Version 10.0.19042.804]
    (c) 2020 Microsoft Corporation. All rights reserved.

    C:\Users\maxwellsmart>ver
    
    Microsoft Windows [Version 10.0.19042.804]  #<-------------  GOOD, THIS VERSION SUPPORTS WSL2
    ```

0. If you do NOT see the current version as shown above, then stop to upgrade to Windows 10. See you in about 20 minutes!

        `Settings` ▸ `Update & Security` ▸ `Windows Update` ▸ `Install now`

0. Repeat the above, rebooting, installing, rebooting, installing, and rebooting and installing until your ancient OS is current! This is worth it!

0. If you have already installed WSL, you **may have the old version**. Check your current version. If it is VERSION 1, you have the OLD version. You want version 2. 

    `PS C:\Users\maxwellsmart>` `wsl -l -v`

    ```
      NAME      STATE           VERSION
    * Ubuntu    Running         1     #<-------------  BUMMER, VERSION 1 (UPDATE REQUIRED)
    ```

    >Why is this relevant?  
    >WSL1 is based on Microsoft's Linux-compatible kernel interface, a compatibility translation layer with no Linux kernel code.  
    >WSL2 is redesigned with a Linux kernel running in a lightweight VM environment, and innovators have found a lot more things they can do with WSL2.  

0. Open Windows PowerShell **Run as an Administrator**.  Enter the command below <a target="_blank" href="https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/what-is-dism"> windows DISM commands</a>

    `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`  
    `dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`  

0. Download and install the Linux kernel update package <a target="_blank" href="https://docs.microsoft.com/en-us/windows/wsl/install-win10"> as per this documentation</a>.

    https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

0. You are going to reboot next, so this would be a great time to change your PC's hostname.  Most likely, your hostname is something like `3DGh23d-dell-eekzonk` which creates a horrible hostname on the linux command line as WSL adopts your Windows 10 hostname as the WSL2 hostname. Normally we always edit out hostname to be something generic like `hostname` or just `host`.

    `Settings` ▸ `System` ▸ `About` ▸ `Rename this PC`
    
    >If you are a course developer, changing your hostname to `host` will mean NEVER editing your hostname again.  When you copy and paste your commands into the lab steps,  your hostname will be `host`.

0. REBOOT

0. Set WSL2 as default.  Open Windows PowerShell **Run as an Administrator**.  Enter this command.  It takes about five minutes to complete.

    `wsl --set-default-version 2`

0. Install ubuntu 20.04 on your Windows Subsystem for Linux (WSL2). <a target="_blank" href="https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab"> Click Here.</a>  

   a. Click Install  
   b. Click the X on the popup window so you don't have to sign in.  

    > You will not be using this application directly! We're only allowing ubuntu to be opened in the Windows terminal.

0. When an ubuntu terminal pops up, you will be prompted for a user name and password. Consider a generic username like ***user*** as your username.  

    `username" `user`
    > Why do this?  
    > Because your linux prompt will now be `user@host` which is akin to John Belushi's COLLEGE shirt, and will speed up your course writing since no edits are necessary from screen shots! 

    ![Dropdown Location](https://labs.alta3.com/images/college.png)
