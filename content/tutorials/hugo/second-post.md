---
title: "Part 2: Uploading Through a Pipeline"
date: 2024-02-29
draft: false
headingPost: "Author: Joe Spizzandre"
weight: 52
---

![image](/images/pipeline-hero.png)

## Hosting Your Hugo Site on GitLab Pages: A Simple Guide

GitLab Pages offers a streamlined, no-cost solution for hosting static websites directly from a GitLab repository. For users of Hugo, the popular open-source static site generator, GitLab Pages can be an ideal platform for publishing content. The process is straightforward, leveraging GitLab's Continuous Integration/Continuous Deployment (CI/CD) pipelines to automate the deployment of your Hugo site. Here, we'll guide you through setting up your Hugo repository on GitLab Pages using a simple `.gitlab-ci.yml` configuration file.

### Prerequisites

Before we dive in, ensure you have:

- A GitLab account.
- A Hugo site in a GitLab repository. If you're new to Hugo, start by creating a site following the [official Hugo documentation](https://gohugo.io/getting-started/quick-start/).

### Step 1: Understanding the `.gitlab-ci.yml` File

The `.gitlab-ci.yml` file is the heart of GitLab's CI/CD process, instructing GitLab on how to build and deploy your site. For a Hugo project, this file is relatively straightforward. Here's a breakdown of the provided configuration:

```yaml
default:
  image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo:latest"

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment: production
```

- `default:image`: Specifies the Docker image to use for the pipeline. Here, it's set to use the latest Hugo image provided by GitLab.
- `variables:GIT_SUBMODULE_STRATEGY`: If your Hugo site uses Git submodules (e.g., for themes), this setting ensures they are properly initialized and updated.
- `pages`: This predefined job builds your pages and saves them to the `public` directory.
  - `script`: The commands to run, in this case, simply `hugo`, which builds your site.
  - `artifacts:paths`: Specifies the directory to preserve after the build, which GitLab Pages uses to serve your site.
  - `rules`: Ensures the job runs only on commits to the default branch (typically `main` or `master`).
  - `environment`: Designates the deployment environment. Here, it's set to `production`.

### Step 2: Adding the `.gitlab-ci.yml` File to Your Repository

1. In your Hugo site's root directory, create a file named `.gitlab-ci.yml`.
2. Copy the provided YAML configuration into this file.
3. Commit and push the file to your GitLab repository:

   ```bash
   git add .gitlab-ci.yml
   git commit -m "Add GitLab CI/CD configuration for Hugo"
   git push
   ```

### Step 3: Watching the Magic Happen

Once the `.gitlab-ci.yml` file is in your repository, GitLab's CI/CD pipeline kicks into action:

1. **Build**: GitLab CI/CD uses the specified Hugo Docker image to run the `hugo` command, building your static site.
2. **Deploy**: The built site, contained within the `public` directory, is then deployed to GitLab Pages.

You can monitor the pipeline's progress in the CI/CD section of your GitLab repository. If everything goes according to plan, your site will be live on GitLab Pages upon completion.

### Accessing Your Site

After a successful deployment, your Hugo site will be accessible at a URL following this pattern: `https://<your-username>.gitlab.io/<repository-name>`. GitLab automatically provides this URL, but you can also configure a custom domain in the Pages section of your project settings if you prefer.

### Conclusion

Congratulations! You've successfully automated the deployment of your Hugo site to GitLab Pages using GitLab's CI/CD pipeline. This setup not only simplifies the publishing process but also encourages a more dynamic and efficient workflow for managing your site's content. As your site grows, you can easily update your `.gitlab-ci.yml` to include additional steps, such as minifying assets or running tests, ensuring your Hugo site remains fast, secure, and easy to maintain.

GitLab Pages offers a robust and cost-effective solution for hosting static sites like Hugo. By leveraging GitLab's CI/CD pipelines, you can automate your deployment process, ensuring your site is always up-to-date with the latest changes from your repository. Whether you're creating a personal blog, a project portfolio, or a documentation site, this guide has equipped you with the knowledge to publish your content on GitLab Pages with ease.