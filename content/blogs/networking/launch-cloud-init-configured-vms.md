---
title: "Launch Cloud Init Configured Vms"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---


### Objective:
1. Learn how to use <a target="_blank" href="https://cloudinit.readthedocs.io/en/latest/"> cloud-init</a> to deploy Ansible-ready VMs.

### Tasks:

1. In WSL2, `cd` to a directory we will use to store cloud-init files. **IMPORTANT**: This entire lab will be done in this directory.

    `$` `cd /mnt/c/Users/<YOURUSERNAME>/Downloads/virtualbox`

0. Create the first file that will be loaded into the cloud-init file. This file is called *meta-data*. 

    `$` `vim meta-data`

    ```
    instance-id: student1
    local-hostname: student1
    ```

0. To generate an SHA-512 password hash, you need to install *whois*.

    `$` `sudo apt-get install whois`

0. Now generate an SHA-512 hash from the password *alta3*. COPY THE OUTPUT!

    `$` `mkpasswd -m sha-512 alta3`
    
    ```
    # YOURS WILL BE DIFFERENT (but this one would work)
    $5$i8125kzzQjONJw$xhsIMB5YVO5DlXZsHu7cmEg5GCwuEnuGdjWej9EULI1
    ```

0. Copy your public RSA key by running this command, and COPY the output with your mouse.

    `$` `cat ~/.ssh/id_rsa.pub`  *Then copy the key with your mouse.*

0. Create the second file of the cloud-init, called *user-data*. You will need to provide the password you created in the previous step on line 2. 

    `$` `vim user-data`
    
    ```
    #cloud-config
    users:
      - name: ubuntu
        shell: /bin/bash
        sudo: ['ALL=(ALL) NOPASSWD:ALL']
        groups: sudo
        lock_passwd: false
        passwd: $6$eNsEznDxuF6fGxO7$E3fNyjGaOJkAudnY1YqsDAraudBMthBj0NYCCmKumy5KRDWfX98HC5PuNIcutwvsGrhnLotjC..45wRG8O2Mg/
        ssh-authorized-keys:
        - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDs9WeVtgZnGl2Qt/uS5j8i2SqIwqNCRScizbGfz+mqXoWTR1Eg5lovwBVODH2DWscFBycoGG/AamLbwF/xhWPXCrs43nQzV0NZmbXMgUIVuvoWh/5ZaUwNFpt3UQbPJKpCKTO67CHo1BQQbkCOPhNF6fPDJCk+jM+O9YFmgYiUDZd/lXbYlWw3+j7P6+ms4i3eZdpDW5G55V7HOlJA5bXi6/w/4KDSgcv54G5Gs2jK/TaR+4n5WeWqAp/Oqzh7fkzIi3F3qcFUYDLKMceRAFkI92e3yDbQVVbQtb3UQMnCTve29QYyOuNECIHPkyd+x9JazJmnhCAhuR8Qu3h0P2RJ chad@Chad
    ```
    - WARNING that first line is **NOT A COMMENT**. If you start the file with `# cloud-config` (note the space between the hash and cloud-config, it WILL NEVER WORK. Many years of time have been lost to this.  
    - passwordless sudo is set up for Ansible  
    - ubuntu is made a sudoer  
    - The password will work (*bad idea for production*, all that is needed in poduction is an RSA key)  
    - The ssh-authorized-keys is your id_rsa.pub key from the previous lab

0. On WSL2 windows terminal, use apt to install *cloud-image-utils*.

    `$` `sudo apt install cloud-image-utils -y`

0. Combine user-data & meta-data into an ISO file. This is your **cloud-init** file!

    `$` `cloud-localds cloud-init.iso user-data meta-data`

0. Download the Ubuntu 20.04 server image we'll need for our VM. After some work, this single file will become your VM's virtual hard drive.

    `$` `wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img`

0. Check the size of the image. You must think of this file as a vitual "hard drive". If you use this file as it is now, when you launch a virtual machine, the hard drive will be 100% full and will not boot. 

    `$` `qemu-img info focal-server-cloudimg-amd64.img`

0. Let's resize the virtual size of the file to give it some wiggle room. This will not actually increase the size of the file, just the MAXIMUM size it can become when it becomes our virtual boot drive. Think of it as similar to <a target="_blank" href="https://en.wikipedia.org/wiki/Thin_provisioning"> thin provisioning</a> like how VMWare may.

    `$` `qemu-img resize focal-server-cloudimg-amd64.img 8G`

0. Confirm the VIRTUAL size has been increased to 8 GB.

    `$` `qemu-img info focal-server-cloudimg-amd64.img`

0. Make sure you are still in the correct directory.

    `$` `pwd`

    ```
    /mnt/c/Users/<your-windows-user>/Downloads/virtualbox
    ```

1. We will now convert this file into a VDI hard disk image that we will use in VirtualBox.

    `<name>@<Name>:/mnt/c/Users/<your-windows-user>/Downloads/virtualbox$` `qemu-img convert -O vdi focal-server-cloudimg-amd64.img focalserver.vdi`

0. Return to your VirtualBox application on your desktop. Click the **NEW** button at the top.

0. In the "Create Virtual Machine" window that opens, click the **Expert Mode** button in the bottom left. If there is no option for *Expert Mode*, you are already there. Then apply the following settings in this window:

    ```
    Name: <your choice>
    Machine Folder: <default is fine>
    Type: Linux
    Version: Ubuntu (64-bit)
    Memory size: <1024 MB is fine>
    Hard disk:
      - select "Use an existing virtual hard disk file"
      - navigate to your /Users/<your-windows-user>/Downloads/virtualbox directory
      - select the focalserver.vdi file you made in a previous step

0. Once you've applied these settings in this window, click the **CREATE** button at the bottom.

0. Once you've returned to the main VirtualBox window, click to select your new VM.  Then click **Settings** in the top of the window.

0. Make the following adjustments in **Settings**:
    - *Storage*- click on "Controller: IDE" and the icon atop it that appears as a CD with a + sign on it. Navigate to your /Users/<your-windows-user>/Downloads/virtualbox directory and select **cloud-init.iso**
    - *Network*- click on the "Attached to" dropdown and select "Bridged Adapter"
    - Click **OK**.

0. You may now click the green **Start** arrow. After a brief loading period, you'll be prompted to provide login and password. If you used the steps provided above, they will be **ubuntu** and **alta3**, respectively.

0. First you'll want to get the ip of the VM by doing an `ip a` on the VM. That ip will be the target for your playbook coming up.

0. Next, verify ssh connectivity between your WSL and the Virtual Machine you just created. 

    `$` `ssh ubuntu@<your_VM_ip>`

0. In your WSL Windows Terminal, run a hello world playbook that makes a change to your target VM. 

   1. Create a hosts file

   0. Create a host_vars file

   0. Write a playbook that writes a hello-world file to the target

   0. Verify that the file is on the target
