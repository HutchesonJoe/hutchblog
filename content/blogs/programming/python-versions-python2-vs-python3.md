---
title: "Python Versions Python2 Vs Python3"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
---
### Python is a generalized term

If you have ever been confused by the term “Python”, or by all the number of Python versions in existence, don’t feel bad. The term “Python” is a bit general in nature. Since the initial debut in 1991, the language has gone through a number of major and minor revisions. The most notable, is the change from Python 2.x to Python 3.x.

Python 2 matured at version 2.7+. This was scheduled to hit end of life years ago, but finally has done so at the start of 2020. See [Sunsetting Python 2](https://www.python.org/doc/sunset-python-2/) if you care to read the official release. TLDR; any machines still running Python 2.7+ should be thought of the same way you’d think of Windows 95. Historic. Old like the dinosaurs.

When people say “python”, you can usually assume they mean Python 3, but you won’t be thought less off if you start the conversation with a question like, “Is this project supported the latest release of Python?”

If the answer to that question is, “Python 2.7+”, you probably want to ask a lot of follow up questions before joining that project. These could include:

*   What libraries within the project actually depend on Python 2?
*   Has anyone tried updating the code to run on Python 3?
*   Moving forward, how does the security team feel about us installing Python 2.7+ on our servers?
*   Will other teams be negatively effected if we dual home Python 2.7+ and Python 3.x onto our servers?

### Python 2 is old, in the extreme

Python 3.x (at the time of writing, Python 3.8.1 is stable) [is documented here](https://docs.python.org/3/). Note the “3” at the end of .org. When you reach for Python documentation, make sure you’re surfing the 3.x pages, and not the 2.x pages. Remember, we want to get rid of Python 2 whenever possible.

### Removing / Uninstalling Python 2

It’s easy to reinstall if you find you ‘break’ something, but it really is time to move away from Python 2.x releases. The following steps outline how to trash Python 2.x.

#### How to remove / uninstall Python 2 on Debian or Ubuntu

Removing Python from your system should be as simple as issuing the following command.

  `$` `sudo apt remove python`

#### How to remove / uninstall Python 2 on Windows

Search the Windows start menu for “Add or Remove Programs”. Select “Python 2.7”, then click “Uninstall”.

### Installing Python 3

If you’re new to Python, or just removed Python 2, then install the latest version of Python 3 to your system, and be on your way! Below are instructions for installing the latest Python versions on Debian, Ubuntu, and Windows systems.

#### How to install Python 3 on Debian or Ubuntu

Python3 and the package installer pip can easily be installed using the aptitude package installer.

  `$` `sudo apt install python3-pip`

#### How to install Python 3 on Windows

Mosey on over to the [Python downloads page](https://www.python.org/downloads/). Download the latest version of Python and run the executable that comes down.

### Updating Python 3

If you already have Python installed on your machine, you have a decision to make. Remove the old version, and install the new version, or install yet another Python version onto your machine.

#### How to update Python 3 on Debian or Ubuntu

The apt package index is a set of available packages from repositories from within /etc/apt/sources.list file and from within in the /etc/apt/sources.list.d directory. To update that list of available packages, type:

  `$` `sudo apt update`

After typing the above command, upgrade all installed packages on your system with:

  `$` `sudo apt upgrade`

#### How to update Python 3 on Windows

You can install multiple versions of Python on Windows. However, I don’t see any reason to collect Python old versions of Python. Therefore, I’ll first uninstall my current version of Python, before running the installer for the latest version. The removal instructions for Python 3 are the same as Python 2.

After updating, dev tools like PyCharm might need their interpreter updated to point to the latest version of Python.

This post orginally appeared at [rzfeeser.com](https://rzfeeser.com/2020/02/05/python-versions-python2-vs-python3/) and has been reproduced with the permission of the author.
~
