---
title: "How To Create A Virtual Machine"
date: 2024-02-22
draft: false
headingPost: "Author: Chad Feeser"
---
# How to Create a Virtual Machine

<video class="slide" controls="" controlslist="nodownload" data-autoplay="" poster="https://labs.alta3.com/courses/common/images/demo-poster.png"><source type="video/mp4" src="https://youtu.be/S0muu92ko00?si=P1aOzgyl3fJF4bcN" data-lazy-loaded=""></video>
