---
title: "Ansible Is For Everyone"
date: 2024-02-22
draft: false
headingPost: "Author: Joe Hutcheson"
---
At first glance, Ansible can be overwhelming. On Ansible.com, Red Hat describes its "open source community project" as "the simplest way to automate IT" and goes on to say: 

>Ansible is an open source IT automation tool that automates provisioning, configuration management, application deployment, orchestration, and many other manual IT processes.

Admittedly, this description may not make Ansible seem any more accessible. In order to simplify things and inspire confidence in students in Alta3 Research's *Ansible 101*, I generally lead with the following three concepts:

- Ansible is meant to be *easy* to use, even if you are not familiar with Python.
- An **Ansible playbook** contains what you want to *do*.
- An **Ansible inventory** describes *to whom* you want to do it. 

Ansible is a "domain specific language", or DSL. A DSL is a programming language with a high level of abstraction optimized for a narrow set of problems. In this case, Ansible is software written in Python. When we write Ansible, we're actually describing a series of pre-written Python scripts we want to execute. However, knowledge of Python programming is not essential to being successful in Ansible. The Python code operates under the surface of Ansible; in our Ansible 101 course, the only time we really see any Python is when the instructor (me) gets overly enthusiastic about a module and insists on looking into the source code. Unless that type of thing thrills you as it does me, you will likely be better off being an Ansible surface-dweller for the time being--a much more economic use of your time and effort. And you can always do a deep dive into the Python code after you get the hang of things. 

While there is an abundance of ready-to-go Ansible modules to handle a multitude of tasks, developers can also create their own modules to incorporate. With most vendors supported by Ansible already, the only reasons to write your own Python module would be supporting in-house services, or legacy systems.

Put simply, Ansible allows you (the controller) to simultaneously reach out to other servers and devices on your network and manipulate their state, i.e. get everyone on the same page at the same time. Ansible must be installed on the controller, but does not need to be installed on the remote servers. With Linux hosts, the default behavior is to execute the Python script(s) on the target hosts, so (at least with Linux targets) Python is a requirement on the target systems, however, Ansible can even install Python for you as a kind of prerequisite step 0.

Ansible is driven by an **inventory**, or a collection of managed nodes (hosts), which can be network servers or devices from a variety of vendors. In Ansible 101, we generally deal with *static inventories* or inventories that won't change over time. We also cover dynamic and hybrid inventories, as a general understanding of these concepts is valuable. 

For the "Python who?" crowd, you can take comfort in the fact that your inventory is not written in Python code either. This file will be written in either YAML or INI format and doesn't contain much programming logic beyond defining some important host variables:

```
# an example of an inventory file written in INI

[guitar]
john      ansible_host=10.10.1.2 ansible_user=john
george    ansible_host=10.10.1.4 ansible_user=george
paul      ansible_host=10.10.1.3 ansible_user=paul

[bass]
pete      ansible_host=10.10.1.6 ansible_user=pete

[drums]
ringo     ansible_host=10.10.1.7 ansible_user=ringo ansible_ssh_pass=alta3
pete      ansible_host=10.10.1.6 ansible_user=pete

[beatles]
john      ansible_host=10.10.1.2 ansible_user=john
george    ansible_host=10.10.1.4 ansible_user=george
ringo     ansible_host=10.10.1.7 ansible_user=ringo
paul      ansible_host=10.10.1.3 ansible_user=paul

[strings:children]
guitar
bass
```

Once you have established your inventory, or the "to whom you want to do things to", it's time to decide what those actions will be. Ansible commands can be run in ad-hoc mode right from the command line with the 'ansible' command. This is only marginally useful, except in a learning environment. More than likely, you will create a **playbook**--run with the 'ansible-playbook' command--which is a collection of tasks you want to run against your inventory. Playbooks are meant to be simple and readable, calling on Ansible modules (written in Python code) to do the heavy lifting. Once you discover how the module works, you can just trust the Python code running underneath without having to understand it. 

Ansible is built to be easy to use. The [Ansible documentation](https://docs.ansible.com/ansible/latest/) is very accessible and readable with plenty of examples. And Ansible is also built to be rather intuitive; the programming automatically looks for things in certain directories where it expects to find them without the user necessarily having to be specific about a path, especially data harvested by the gather_facts module, a module which is automatically called by playbooks to gather useful variables about remote hosts. Even with more complex concepts important to Ansible beginners (variable precedence, I'm looking at you), there is an internal logic at work that feels very intuitive and starts to just make sense. 

For any software development project, regardless of the size or ambition, the ultimate goal is to create programs which are user friendly and easy to employ without relying on an understanding of what is going on beneath the surface. Ansible is one great big successful example of this endeavor. 
