---
title: "Tune Your Dot Files"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---

### Objectives:
1. Make your environment easy for students to follow.
2. Remove the irritating bell sounds that compete with the lesson and your sanity.

### Tasks:

1. Make your bash prompt interesting (read gaudy). It's helpful for my students to track a vivid prompt to delimit my activity. [Go here to create the perfect bash prompt](https://ezprompt.net/)
    
    My favorite: `export PS1="\[[33m\]\u\[[m\]\[[35m\]@\[[m\]\[[36m\]\h\[[m\]\[[35m\]:\[[m\]\[[32m\]\w\[[m\] \[[35m\]\$\[[m\] "`

    ![Gaudy-Prompt](https://labs.alta3.com/images/prompt_colors.png)

0. Once you like what you see, save the prompt at the bottom of your `.bashrc` file like this:  `:wq` to save and quit, OR `q!` to quit without saving if you made a mistake. 

    `~ $` `vim .bashrc`

    ```
    PS1="\[[33m\]\u\[[m\]\[[35m\]@\[[m\]\[[36m\]\h\[[m\]\[[35m\]:\[[m\]\[[32m\]\w\[[m\] \[[35m\]\$\[[m\] "
    ```

0. Set vim as the default editor.

    `~ $` `sudo update-alternatives --config editor`  

    `3` Select option 3

0. Remove the irritating CLI BELL by editing the /etc/inputrc file. (Yes, this is repeated from the last lab).

    `~ $` `echo 'set bell-style none' | sudo tee -a /etc/inputrc`

0. Get rid of the vim bell. This creates a .vimrc as it will not be there on new installations.

    `~ $` `vim .vimrc`  

    ```
    set belloff=all
    ```

### Create your Github Repository!

1. Go to https://github.com/yourUsername

0. Click on **Repositories** at the top.

0. Click **New** on the right-hand side.

0. Fill in your Repository name, make it private, then click the checkbox to make a README file.

0. Now click **Create Repository**.

    ![private_github_repo](https://labs.alta3.com/images/private_repo.png)

### Backup your dotfiles!

1. Create a bare repository called `.dotfiles`

    `~ $` `mkdir $HOME/.dotfiles`  

    `~ $` `git init --bare $HOME/.dotfiles`

0. Create an alias to issue git commands while NOT being inside the .dotfiles directory.

    `~ $` `alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'`

0. Add the dotfiles alias to your .bashrc.

    `vim .bashrc`

    ```
    alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
    ```
    
0. This is important for your understanding.  From here on out, when you type `dotfiles` it is like typing:

    `git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'   PLUS WHATEVER ELSE YOU TYPE`

0. Don't show untracked files, as there will be too many in your $HOME directory!  

    `dotfiles config --local status.showUntrackedFiles no`

0. Target your github account, NOT `sfeeser`!

    `dotfiles remote add origin git@github.com:sfeeser/.dotfiles.git`

    > If you accidentally run the command exactly as above, you can fix that with `dotfiles remote remove origin` and then update the command to your github account.

0. Add the .bashrc file you just created.

    `~ $` `dotfiles add .bashrc`

0. Purely FYI, the above command = `git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME add .bashrc` 

0. Tell github who your are.

    `~ $` `git config --global user.email "youremail@address"`  
    `~ $` `git config --global user.name "yourusername"`  

0. Commit your change.

    `~ $` `dotfiles commit -m "add .bashrc"`

0. Push your changes.

    `~ $` `dotfiles push --set-upstream origin master`

### Recovery at a new machine

1. Pull down the `.dotfiles` using your account, **not** sfeeser.

    `~ $` `git clone --separate-git-dir=$HOME/.dotfiles https://github.com/sfeeser/.dotfiles.git ~`
    
0.  If the command above doesn't work, clone to a temp directory and use rsync to bring it all back. Use your account, **not** sfeeser.

    `~$` `git clone --separate-git-dir=$HOME/.dotfiles https://github.com/sfeeser/.dotfiles.git tmpdotfiles`
    
    `~$` `rsync --recursive --verbose --exclude '.git' tmpdotfiles/ $HOME/`
    
    `~$` `rm -r tmpdotfiles`
