---
title: "Vim Introduction Getting Started With Vim"
date: 2024-02-22
draft: false
headingPost: "Author: Timothy Patrick"
---
# VIM Introduction | Getting Started with VIM

<video class="slide" controls="" controlslist="nodownload" data-autoplay="" poster="https://labs.alta3.com/courses/common/images/demo-poster.png"><source type="video/mp4" src="https://youtu.be/y7cWMV0jLWU?si=-G1SMlhSRczInS4E" data-lazy-loaded=""></video>
