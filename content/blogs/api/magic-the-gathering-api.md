---
title: "Magic The Gathering Api"
date: 2024-02-22
draft: false
headingPost: "Author: Joe Hutcheson"
---
Fortunately for developers, well-trafficked API's will often provide us with something called a Software Developer Kit (SDK) to make our already complicated jobs somewhat easier. SDK's are essentially a toolbox for developers, the tools therein being prewritten code to expedite the process of getting that information from that API and into our applications.

Take, for example, the [Magic the Gathering API](https://magicthegathering.io/), developed and maintained by Andrew Backes. With this API, developers can access information on cards, sets, and a number of other components of this popular card game of wizardry. But you don't have to know much about the game to appreciate how accessible this API has been made for developers.

Even without utilizing the SDK, we can build API calls referencing the clean and readable documentation. For example, we can format endpoints for requests as follows:
```
https://api.magicthegathering.io/<version>/<resource>
```
A request for all of the cards available would be sent to this endpoint:
```
https://api.magicthegathering.io/v1/cards
```
In Python, we could set up our GET request by installing and importing the 3rd party *requests* library, which allows us to strip off the JSON into a Pythonic dictionary. The information we want actually lives under the key "cards", but the information on each card is a somewhat dense. Lets call the "name" key from each card just to get some cleaner output:
```
import requests

MTG_API_cards = "https://api.magicthegathering.io/v1/cards" # this returns a maximum of 100 cards, per the documentation

def main():
    mtg_response = requests.get(f"{MTG_API_cards}cards").json() 
    cards = mtg_response["cards"] # the cards can be found at the "cards" key in the dict
    for card in cards:
        print(card["name"])
       
if __name__ == "__main__":
    main()
```
The output of this code would be:
```
Ancestor's Chosen                                                         
Ancestor's Chosen                                                         
Angel of Mercy                                                            
Angel of Mercy                                                            
Angelic Blessing                                                          
Angelic Blessing
...
```
Clearly some duplicates of cards exist, but we won't worry about that right now; cleaning those up would be easy if we needed to. More importantly, there's an easier approach to this provided by our Python SDK. Incidentally, the MTG API has kits for several languages, including Ruby, JavaScript, GO, and TypeScript.

Our Python SDK needs to be installed from the command line:
`pip install mtgsdk`

In our script, we can import the module we would need: 
```
from mtgsdk import Card
```
Now, we don't have to hard code an HTTP request. Our API call above automatically limits us to 100 responses. But there are a multitude of iterations of the Card class. To limit the response back, the SDK gives us some instructions for specifying a page of data, as well as other query parameters.
```
cards = Card.where(page=5).where(pageSize=1000).all()
```
This gives us a list of all of the iterations of the Card class over which we can loop. The documentation gives us the properties that are available in our Card class; since *name* is one of them, we no longer call it as a key, but as an attribute:
```
for card in cards:
    print(card.name)
```
The complete script could look like this:
```
from mtgsdk import Card

def main():
    cards = Card.where(page=5).where(pageSize=1000).all()
    for card in cards:
        print(card.name)

if __name__ == "__main__":
    main()
```
Many of the more established and well-maintained open API's will provide developers with an SDK in at least a few of the major languages. And just like with most every other product, the easier an API is to work with, the more likely developers will want to use it. 

Hopefully SDKs are a bit clearer. If you are interested in a study of SDKs and APIs, consider doing it with [Alta3 Research's API and API Design with Python](https://alta3.com/overview-api-python-training) course!
