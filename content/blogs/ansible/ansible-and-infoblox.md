---
title: "Ansible And Infoblox"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
---

Using Ansible to interact with Infoblox is *easy*. Ansible has an <a href="https://docs.ansible.com/ansible/latest/scenario_guides/guide_infoblox.html">Infoblox Guide</a> on their website.  But here are the steps to controlling Infoblox with Ansible.

{{< youtube liFI3w33tUE >}}


### Step 1 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=10s"> Install Python on your favorite Linux distribution</a>

  `Debian / Ubuntu:` `sudo apt install python3-pip -y`

  `RedHat:` `sudo yum install python3-pip -y`

### Step 2 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=13s"> Install Ansible via pip</a>


  `Debian / Ubuntu:` `python3 -m pip install ansible`

  `RedHat:`  `python3 -m pip install ansible`

### Step 3 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=19s"> Install the infoblox-client via pip</a>

  `Debian / Ubuntu:` `python3 -m pip install infoblox-client`

  `RedHat:`  `python3 -m pip install infoblox-client`

### Step 4 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=33s"> Take a quick look into your Infoblox Management Interface</a>

> Depending on your system you may need to set up some port-forwards to see the Management Interface. 

### Step 5 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=49s"> Create a playbook</a>

Below is an example of a playbook supported by Ansible version 2.10 using Python version 3.6. We'll refer to this file as `infoblox-playbook02.yml`

    
    ---
    - name: DNS Zone, Network, and Host Play
      hosts: localhost
      connection: local
      gather_facts: no   # running against local host

      vars:
              domain: campuswest.local
              dcomment: local DNS zone
              state: present
              networktocreate: 192.168.200.0/24
              netcomment: this is my second IPv4 network
              host: hotfudge

      vars_files:
              - vars/infoblox.creds   # contains nios_provider

      tasks:
              - name: "Create a forward DNS zone called {{ domain }}"
                nios_zone:
                  name: "{{ domain }}"
                  comment: "{{ dcomment }}"
                  state: "{{ state }}"
                  provider: "{{ nios_provider }}"    # credentials
      
              - name: Attempting to create a network on the default network view
                nios_network:
                        network: "{{ networktocreate }}"
                        comment: "{{ netcomment }}"
                        options:
                                - name: domain-name
                                  value: "{{ domain }}"
                        state: "{{ state }}"
                        provider: "{{ nios_provider }}"
                              
              - name: display what is returned from a lookup plugin
                debug:
                    var: lookup('nios_next_ip', networktocreate, provider=nios_provider)
                    verbosity: 1

              - name: "configure an IPv4 host record for {{ host }}.{{ domain }}"
                nios_host_record:
                  name: "{{ host }}.{{ domain }}"
                  ipv4addrs:
                    - ipv4addr:
                        "{{ lookup('nios_next_ip', networktocreate, provider=nios_provider)[0] }}"
                  state: "{{ state }}"
                  provider: "{{nios_provider}}"


### Step 6 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=52s"> Review the code inside your playbook</a>

### Step 7 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=177s"> Create a directory for the file used to secure your credentials</a>

We already have this setup from previous ansible testing.  If you don't have the same setup, you can follow the options documented below to set up your vault.

  `Debian / Ubuntu:` `mkdir vars/`

  `RedHat:`  `mkdir vars/`

We place these credentials in an external file, `vars/infoblox.creds` to avoid exposing our credentials within our Playbook. An example of this credential file can be found below.

    # Ansible variable file containing Infoblox creds
    nios_provider:
        host: 192.168.10.55      # IP of an instance of Infoblox
        username: admin          # username for infoblox
        password: LeR0Y!Jenkinz! # password for infoblox

### Step 8 -  <a target="_blank" href="https://www.youtube.com/watch?v=liFI3w33tUE&t=186s"> Try running your playbook</a>

We are skipping on the vault for this run because we want to make sure our syntax and formatting are correct. Then we can get back to security!

  `Debian / Ubuntu:` `ansible-playbook infoblox-playbook02.yml`

  `RedHat:`  `ansible-playbook infoblox-playbook02.yml`

Once things are working, you'll want to use `ansible-vault` to encrypt your credentials. If you run into any problems, be sure to read the error message.  Ansible is actually quite good at offering explanations as to why something didn't work. If you can't figure it out, try to Google the error. Chances are, someone has already posted a solution.

If you want Ansible training, check out the <a href="https://alta3.com/overview-ansible">Alta3 Research Ansible course overview</a>. Training is available to individuals, as well as to companies seeking group training or certifications.
