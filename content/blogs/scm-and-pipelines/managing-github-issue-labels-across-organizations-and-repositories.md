---
title: "Managing Github Issue Labels Across Organizations And Repositories"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---
GitHub's default issue labels are really, really bad.

Our GitHub-issue-centric organization suffered under these non-descriptive labels for far too long, so we finally took up the charge for fixing the 'issue' once and for all!

In addition to poor defaults, GitHub does not provide the ability to manage issue labels to the degree and fidelity that most users and organizations need. [[1](https://github.com/isaacs/github/issues/371)] [[2](https://github.com/isaacs/github/issues/281)] Some things you can't do with [GitHub's default label management](https://help.github.com/en/github/setting-up-and-managing-organizations-and-teams/managing-default-labels-for-repositories-in-your-organization) include:
 - Updating default labels for old repos (only new ones)
 - Managing labels across multiple organizations or users
 - Revision control and management of the changes
 - Making backups of existing labels on existing repos

Our solution used the awesome ["Labels"](https://github.com/hackebrot/labels) Python project in combination with a short Python script that used the ["pygithub"](https://github.com/PyGithub/PyGithub) library.  In the end, we had our old labels backed up and we established a new default issue label scheme that will likely last us for many years to come.  Below are the resulting issue labels, the steps we followed, and the script and defaults so you can do the same thing for your repos.


![Alta3 Default GitHub Issue Labels](https://static.alta3.com/blog/github-labels.png)

### Prerequisites

- User has access to all repositories that will be modified.
- A GitHub [personal access token](https://github.com/settings/tokens) has been created.
  - Give it a descriptive name.
  - **Only** select the `repo` scope.

### Install


These steps set up the Python environment and configure it:

```
python3.8 -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip wheel
python -m pip install -r requirements.txt
```

**Example `.env` file:**

```
GITHUB_USER_TOKEN="<YOUR_TOKEN_HERE>"
LABELS_TOKEN=${GITHUB_USER_TOKEN}
LABELS_USER="<YOUR_USERNAME>"

```

### Run

**List All Alta3 Repos**

Take a minute to update this script to fetch the lists of users' and organizations' repositories that you would like to manage. We will use this list in the next step!

```
python list-all-repos.py
```

**Make a Backup of All Current Labels**

Here we pipe the resulting list to run the `labels fetch` command on each repoisitory

```
mkdir -p repo-labels
python list-all-repos.py | xargs -I {} labels fetch --owner alta3 --repo {} --filename repo-labels/{}-labels.toml
```

**Set up a Common Set of Labels for Every Repo**

View/edit `default-labels.toml` for the list of labels.  Next, we use the same list of repositories to run the `labels sync`. This updates each repository to use our new list of issue labels.

```
python list-all-repos.py | xargs -I {} labels sync --owner alta3 --repo {} --filename all-repos-labels.toml
```

Now looking at each repository we should see a list of available labels like the image above. In addition to this we also created a folder full of all the old labels. If we run this script on a regular basis not only can we keep track of how labels are being created across all of these repos, but also take the good ideas and issue labels from one project and expand their use across all the others in our list.

The source code that was used to accomplish this task for Alta3's repos is available [here](https://github.com/alta3/github-label-management).  Thanks for reading and happy label making!
