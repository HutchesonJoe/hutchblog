---
title: "-28 Bytes Later"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---
When calculating ICMP payload size for MTU testing, you are asked to subtract 28 bytes. For instance, if you want to test MTU transmission of a 9000 byte jumbo frame, you perform the following math:

> 9000 - 28 = 8972. 

So where does the 28 bytes come from?  

The answer can be found in [RFC 792](https://tools.ietf.org/html/rfc792), but the following diagram clearly shows where that 28 bytes originates:  
<br/> <br/> 




          ---------------- +-----------------------------------------------------------------------------------------------+
          ^          ^     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|
          |          |     +-----------------------------------------------------------------------------------------------+
          |          |     |              |        |                       |                                               |
          |          |     | IP Version   | Hdr Len|th   Type of Service   |                 Total Length                  |
          |          |     |              |        |                       |                                               |
          |          |     +--------------+--------+--------------------------------+--------------------------------------+
          |          |     |                                               | FLAGS  |                                      |
          |          |     |                 Identification                +--------+            Offset                    |
          |          |     |                                               |DF|MF|RB|                                      |
          |          V     +-----------------------+-----------------------------------------------------------------------+
          |     IP Header  |                       |                       |                                               |
          |     20 Bytes   |     Time To Live      |        Protocol       |            Header Checksum                    |
          |          |     |                       |     1=ICMP (ping)     |                                               |
          |          |     +-----------------------+-----------------------+-----------------------------------------------+
          |          |     |                                                                                               |
          +          |     |                                      Source IP Address                                        |
        9000         |     |                                                                                               |
        Bytes        |     +-----------------------------------------------------------------------------------------------+
          +          |     |                                                                                               |
          |          |     |                                   Destination IP Address                                      |
          |          v     |                                                                                               |
          |         ------ +-----------------------+------------------------+----------------------------------------------+
          |          ^     |                       |                        |                                              |
          |          |     |          Type         |          Code          |                   Checksum                   |
          |        ICMP    |                       |8=ping req, 0=ping reply|                                              |
          |       Header   +-----------------------------------------------------------------------------------------------+
          |      8 Bytes   |                                                                                               |
          |          |     |                                             Unused                                            |
          |          V     |                                                                                               |
          |         ------ +-----------------------------------------------------------------------------------------------+
          |          |     |                                                                                               |
          |        8972    |                         Data size must be calculated & determined by you!                     |
          |        BYTES   |           Note that the IP header is 20 bytes and the IMCP header is 8 bytes                  |
          v          |     |                                       8972 Bytes                                              |
          ---------------- +-----------------------------------------------------------------------------------------------+
