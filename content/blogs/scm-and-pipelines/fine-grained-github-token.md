---
title: "Creating a Fine-Grained Token in Github"
date: 2024-02-29
draft: false
headingPost: "Author: Sean Barbour"
---
[Fine-grained tokens](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#fine-grained-personal-access-tokens)

Fine-grained personal access tokens have several security advantages over personal access tokens (classic):

- Each token can only access resources owned by a single user or organization.
- Each token can only access specific repositories.
- Each token is granted specific permissions, which offer more control than the scopes granted to personal access tokens (classic).
- Each token must have an expiration date.
- Organization owners can require approval for any fine-grained personal access tokens that can access resources in the organization.

Fine-grained tokens are repository-scoped tokens suitable for personal API use and for using Git over HTTPS.

[Classic tokens](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#personal-access-tokens-classic)
Personal access tokens (classic) are less secure. However, some features currently will only work with personal access tokens (classic):

- Only personal access tokens (classic) have write access for public repositories that are not owned by you or an organization that you are not a member of.
- Outside collaborators can only use personal access tokens (classic) to access organization repositories that they are a collaborator on.
- Some REST API operations are not available to fine-grained personal access tokens. For a list of REST API operations that are supported for fine-grained personal access tokens, see "Endpoints available for fine-grained personal access tokens".

If you choose to use a personal access token (classic), keep in mind that it will grant access to all repositories within the organizations that you have access to, as well as all personal repositories in your personal account.

As a security precaution, GitHub automatically removes personal access tokens that haven't been used in a year. To provide additional security, we highly recommend adding an expiration to your personal access tokens.



We use a classic token for access to all repositories in order to be able to pull down changes (think Hot-fix). 

## Objective

Successfully create a token (fine-grained) for use during the installation of Alta3's cloud infrastructure. This will allow read access to the selected repositories that need eyes on to complete installation and running of containers successfully.

1. Go to github.com

0. Click your icon in the top right.

0. Click Settings.

0. Scroll down and click on developer settings

0. Click Personal access tokens (which currently has a dropdown)

0. Click on Fine-grained tokens

0. Click Generate new token

0. Confirm access with your passkey / github mobile / password

0. Section I: **New fine-grained personal access token**

    ![image](https://github.com/alta3/infrastructure/assets/35880398/2d63acf6-0a7f-4903-b6e6-ab6f7788d941)
    
    - **Token Name**: enchilada-install-YYYY-MM-DD
    - **Expiration**: 90 days (default is 30 days)
    - **Description**: "token for downloading the following repos: labs, enchilada, alta3-ansible, infrastructure, gutenberg, demo"
    - **Resource Owner**: alta3 (default is your user)
    - **Write**: "Needed for enchilada installation on cloud" (gets sent with the request for access)

0. Section II: **Repository access**

    ![image](https://github.com/alta3/infrastructure/assets/35880398/3f5a5837-4b26-4f91-8cd6-413cac6fd9b0)

0. Click the bubble **Only select repositories**

0. Click the dropdown **Select repositories**, then select repos:
    
    - [ ] labs
    - [ ] alta3-ansible
    - [ ] enchilada
    - [ ] infrastructure
    - [ ] demo
    - [ ] gutenberg

    ![image](https://github.com/alta3/infrastructure/assets/35880398/6f26d81a-2dea-4757-a2e6-e9d72f41b562)

0. Section III: **Permissions**

0. Click the dropdown for **Repository Permissions**

    ![image](https://github.com/alta3/infrastructure/assets/35880398/37f57a6e-86a8-4bc2-b8ec-d518b2afa37e)

0. Select **Read-only** for:

    - [ ] Contents
    - [ ] Metadata

    > DO NOT Change any other access. After you do this, if you collapse the dropdown, the **Repository permissions** should look like this:

    ![image](https://github.com/alta3/infrastructure/assets/35880398/ef0fb775-a1fa-445d-9143-a9a2a1f382b6)

0. Do not make any changes to the **Organization permissions**.

0. Section IV: **Overview**

0. At the end of the metadata for this token, You should see the following:

    ![image](https://github.com/alta3/infrastructure/assets/35880398/c4731fa7-4835-470a-a38a-9971069d5a93)

    - 2 permissions for 6 of your repositories
    - No change in permissions for your organization
    - The date the token will expire

0. Click on **Generate token and request access**.

    > The reason "and request access" is added onto the button is because you changed the **Resource Owner** to *alta3*. 

0. Send a message to one of the github organization's administrators, requesting they approve the token because you are about to install enchilada on a new cloud!!!

