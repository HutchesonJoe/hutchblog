---
title: "Ansible Switch Config Archive"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/Hje6W_b0qi4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ansible is able to change the configuration of network devices. In this exercise, we will change the configuration of an Arista Switch.

### Step 1 -  <a target="_blank" href="https://www.youtube.com/watch?v=Hje6W_b0qi4&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=2&t=0s">cat the ansible hosts file</a>

`student@beachhead:	$` `cat hosts`

    ```
    172.16.2.10
    172.16.2.20
    ```

### Step 2 - <a target="_blank" href="https://www.youtube.com/watch?v=Hje6W_b0qi4&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=2&t=1s">cat the ansible playbook that you will study line by line</a>

  `student@beachhead:` `cat net02.yml`
  
  ```
  - Arista Config Saver
    hosts: all 
    gatherfacts: false 
    vars :
      ansibleconnection: network_cli 
      ansiblenetworkos: eos 
      ansible become: yes 
      ansible_become_method: enable 
      ansibleuser: admin 
      ansible ssh 
      pass: alta3
  
    tasks:
    - name: Arista Fact Collection 
      eos_facts:
        gathersubset: all  # IMPORTANT! Look at the Return values at the bottom of the eosfacts
    - name: Create Arista config save directory
      file: path=arista_config/ state=directory # Playbook will FAIL without this directory
    - name: Arista Config Save
      shell: "echo '{{ ansible net config }}' > arista config/{{ ansible net hostname }}"
    - name: Arista Archival Process 
      archive:
        path: arista config/{{ ansible net hostname }}	# location of our archived file
        remove: True       # removes the original file
  ```
  
 ### Step 3 - <a target="_blank" href="https://www.youtube.com/watch?v=Hje6W_b0qi4&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=2&t=409s">Edit the last task to create an archive in addition to a plain text file.</a>

  `student@beachhead:` `cat net02.yml`
  
  ```
  - Arista Config Saver
    hosts: all 
    gatherfacts: false 
    vars :
      ansibleconnection: network_cli 
      ansiblenetworkos: eos 
      ansible become: yes 
      ansible_become_method: enable 
      ansibleuser: admin 
      ansible ssh 
      pass: alta3
  
    tasks:
    - name: Arista Fact Collection 
      eos_facts:
        gathersubset: all  # IMPORTANT! Look at the Return values at the bottom of the eosfacts
    - name: Create Arista config save directory
      file: path=arista_config/ state=directory # Playbook will FAIL without this directory
    - name: Arista Config Save
      shell: "echo '{{ ansible net config }}' > arista config/{{ ansible net hostname }}"
    - name: Arista Archival Process 
      archive:
        path: arista config/{{ ansible net hostname }}	# location of our archived file
        remove: False       # <--- THE ONLY CHANGE
  ``` 
  
  ### Step 4 - <a target="_blank" href="https://www.youtube.com/watch?v=Hje6W_b0qi4&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=2&t=436s">Run the playbook</a>
  
  `student@beachhead:` `ansible-playbook -i hosts net02.yml`
  
  ```
  PLAY [Arista Config Saver] ***************************************************

  TASK [Arista Fact Collection] ************************************************
  ok: [172.16.2.20] 
  ok: [172.16.2.10]

  TASK [Create Arista config save directory] ***********************************
  changed: [172.16.2.20]
  ok: [172.16.2.10]

  TASK [Arista Config Save] ****************************************************
  changed: [172.16.2.10] 
  changed: [172.16.2.20] 
  
  TASK [Arista Archival Process ************************************************
  changed: [172.16.2.10]
  changed: [172.16.2.20]
  
  PLAY RECAP ******************************************************************* 
  172.16.2.10	: ok=4	changed=2	unreachable=0	failed=0
  172.16.2.20	: ok=4	changed=3	unreachable=0	failed=0
  ```

### Step 5 - <a target="_blank" href="https://www.youtube.com/watch?v=Hje6W_b0qi4&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=2&t=508s">List the saved files. It worked!</a>

  `student@beachhead:` `ls`

  ```
  hosts net02.yml
  ```

  `student@beachhead:` `cd arista_config/`

  `student@beachhead:-/net02/arista_ config` `ls`

  ```
  SW1 SWl.gz SW2 SW2.QZ
  ```
