---
title: "Ansible Vault Hello World"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---
The [ansible documentation](https://docs.ansible.com/ansible/latest/user_guide/vault.html) regarding Ansible Vault would be perfect if there was a practical example on how to use Vault in a playbook. Just follow the steps below as a "hello-world" example, then read the Ansible documentation and things will make a lot more sense.  

In this example, common passwords normally added in the command line are being placed into Vault, requiring that you only add the Vault password to the command line. For instance, suppose you are configuring a target machine.  There are no RSA keys, so you will need three passwords in addition to your Ansible user. You would have to key the following information on the command line:

    ansible_user: <USERNAME>
    ansible_become_pass: <PASSWORD> 
    ansible_ssh_pass: <PASSWORD>
    ansible_sudo_pass: <PASSWORD>
    
There's a much better way! Just add these variables to Vault and you won't be required to type them on the command line.  The steps below will walk you through this exact scenario.


**Objective:**  By example only, show a practical way to use Ansible Vault from creation to deployment. These steps are a small portion of what Vault can do, but typically, this is all you need. 


1. **Create** a Vault file called `topsecret.yml`. Of course you can name the Vault file whatever you wish, this is just an example.

    ```
    $  ansible-vault create topsecret.yml`
       New Vault password: PASSWORD
       Confirm New Vault password: PASSWORD
    ```        

0. Vim will appear, so populate the file with classic yaml dictionaries as follows. *Make sure to replace PASSWORD with an actual password!*

    ```
    mysql-pwd: PASSWORD
    ansible_user: PASSWORD
    ansible_become_pass: PASSWORD 
    ansible_ssh_pass: PASSWORD
    ```

    > **Important!** - Variables you would normally use on the command line work well by including them in your Vault file. This is often a better option than many "ask-pass" command line arguments. 

0. Press <kbd>Escape</kbd>, type `:wq`, and then press <kbd>Enter</kbd> to save the file.

    ```
    :wq
    ```

0. If you make any changes to the information in the file, you can **edit** a Vault file called `topsecret.yml` by opening it as follows:

    ```
    $  ansible-vault edit topsecret.yml
       Vault password: PASSWORD
    ```

0. You are in vim, so edit the file exactly as if it were a vars_file.

    ```
    mysql-pwd: PASSWORD
    ansible_user: PASSWORD
    ansible_become_pass: PASSWORD 
    ansible_ssh_pass: PASSWORD
    ```

0. Press <kbd>Escape</kbd>, type `:wq`, and then press <kbd>Enter</kbd> to save the file.

    ```
    :wq
    ```

0. **Add** a Vault file called topsecret.yml into your playbook as a vars_file. Now you will NOT have to enter this filename everytime you need to run the playbook.

    ```
    - name: some play
      hosts: somehosts
      vars_files:
      - topsecret.yml
    ```

0. **Run** the playbook called main.yml, that has topsecret.yml included as a `vars_file`. *Note: You will have to use the flag `--ask-vault-pass` to be prompted for the password to open the Vault file.*  

    ```
    $ ansible-playbook  main.yml --ask-vault-pass
      Password: PASSWORD
    ```

### Conclusion

Now a single Vault password can replace many passwords on the command line. You can save the Ansible Vault file with the project and your passwords will be secure, given your Vault password is not compromised. Treat the Ansible Vault file like it was a vars_file in playbooks. The Vault password will be needed to unlock it.  It's a good idea to read the [ansible documentation](https://docs.ansible.com/ansible/latest/user_guide/vault.html) as this will make a lot more sense.
