---
title: "Go Programming - Printing to Standard Out"
date: 2024-02-22
draft: false
headingPost: "Author: Joe Hutcheson"
---
Printing to the console can feel like a strangely unfamiliar task if you are approaching Go from the perspective of other common programming languages. For example, *print()*-ing in Python, *console.log()*-ing in Javascript, or *puts*-ing in Ruby, all have built-in ways to handle this simple procedure. Once you become more familiar with Go (also expressed as Golang or Go lang), it will also feel like a simple procedure. But maybe not at first.

Go describes itself as "a fast, statically typed, compiled language that feels like a dynamically typed, interpreted language." While this may be accurate, it may not seem so upon first encountering Go. Unless you are very familiar with C or C++, Go might feel like a whole new ballgame. 

Very simply put, Go programs are made up of packages. In order to simply print something to the console, we have to make use of a package called *fmt*; packages are imported by the name as a string, i.e. "fmt". According to the Go standard library documentation, the fmt package "implements formatted I/O with functions analogous to C's printf and scanf." If you're a C programmer, that might be a sufficient explanation. If you're not, read on.

*If you're just getting started with Go or don't have it installed on your system, you can experiment with the following code or create your own in the [Go Playground](https://go.dev/play/). It takes a bit of setup and know-how to get Go code to run in your console.*

Here is how we get started with our fmt package:

```
package main

import "fmt"

```

Once we have the fmt package imported, there are a number of things we can do with it. But let's focus on just getting something printed out to the console. We'll start with the simplest method: **Print()**.

``` 
func main() {
  name := "Alta3 Research"
  fmt.Print(name, "
")
}

```

Passing the "
" as the second argument creates a new line and cleaner output. Try running the code with and without to see the difference. We can also print more than one item and affect how it renders to the console by including a string of one space between our items and the new line syntax at the end.

```
func main() {
  firstName, lastName := "Alta3", "Research"
  fmt.Print(firstName, " ", lastName, "
")
}
```

Both of the above examples will produce the same output:

`Alta3 Research`

The **Println()** function works similarly, but adds whitespace automatically between the arguments, and automatically creates the new line. The same output can be achieved using the Println() function as follows:

```
func main() {
name := "Alta3 Research"
  fmt.Println(name)
}
```

OR

```
func main() {
  firstName, lastName := "Alta3", "Research"
  fmt.Println(firstName, lastName)
}
```

Both of these options produce the same output as above (`Alta3 Research`), but neither require the additional arguments to maintain the clean formatting of the output. 

The **Printf()** allows us to insert variables into a string, formatting them based on the given formatting verbs. While there are more than a handful of possible formatting verbs, we will look at the *%v* verb which prints the value of the argument, and the *%T* verb which prints the type of the argument. If I need to reveal the data type of my firstName and lastName, I could do the following:

```
func main() {
  firstName, lastName := "Alta3", "Research"
  fmt.Printf("firstName type: %T
", firstName)
	fmt.Printf("lastName type: %T
", lastName)
}
```

In the example above, the new line syntax is included after the %T verb since the Printf() function will not do it for us. More commonly, Printf() will be used as follows: 

```
func main() {
  firstName, lastName := "Alta3", "Research"
  fmt.Printf("Welcome to %v %v Go traning!", firstName, lastName)
}
```

This code prints a nice welcome message to the console: `Welcome to Alta3 Research Go training!` We've only scratched the surface of the capacity of the fmt package. Much like the JavaScript console object, the Go fmt package goes beyond printing simple messages to the browser. Once you get the hang of these functions, you can begin [exploring the many other functions and verbs](https://pkg.go.dev/fmt@go1.20.2) that are available to Go developers. 
