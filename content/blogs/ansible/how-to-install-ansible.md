---
title: "How to Install Ansible"
date: 2024-02-22
draft: false
headingPost: "Author: Joe Hutcheson"
---

Just a few notes before we get started installing Ansible

Ansible will only run on Linux hosts
Ansible only needs to be installed on the controller or the control node, which refers to the system that needs to run tasks on the target hosts
Ansible is written in Python, so Python must be installed before we try to install Ansible
Linux - Install Instructions
The options for installing Ansible may be dictated by your system. For example, the "apt" command in Linux is a utility for managing packages on Ubuntu, Debian, and related Linux distributions and must be run with sudo (root) privileges; the "yum" command (besides being a bit more amusing) accomplishes the same for Red Hat Enterprise Linux software packages. However, both will typically install older versions of Ansible. If we want the latest version of Ansible, we need to pull the code and build it ourselves, or use the Python package install tool, pip.

Since Ansible is written in Python, you'll need Python installed on your system. Generally the fastest way forward on this is to use "apt" or "yum":

sudo apt install python3-pip -y

Or, if you're working on a RedHat based system:

sudo yum install python3-pip -y

Regardless of which command you use, they both ensure your system has the latest version of Python as well as pip, the Python package manager.

Using pip, the defacto tool for installing Python packages outside of the standard library, will ensure that you have installed the most recent stable release of Ansible. The summary page for pip & ansible may be found here https://pypi.org/project/ansible/

If you have python3-pip installed on your system, the following command will do the trick. The inclusion of the --user flag places the packages in your home directory.

python3 -m pip install ansible --user

Curious why we we are using "python3 -m" in front of "pip install ansible"? If you are on a system with only one version of Python installed, or working from a venv, "pip install ansible" is fine. However, in the case the someone may have additional version of Python installed, we want to ensure we are installing Ansible to the "python3" version we just installed. In effect, this command is read as "python3 I want you to use the module pip to install ansible for the local user".

Once installed, the following command will ask pip to reveal the version:

python3 -m pip show ansible

The above command will output something similar to the following:

Name: ansible
Version: 7.3.0   # <-- your version will likely be higher than this
Summary: Radically simple IT automation
Home-page: https://ansible.com/
Author: Ansible, Inc.
Author-email: info@ansible.com
License: GPLv3+
Location: /home/student/.local/lib/python3.10/site-packages
Requires: ansible-core
Required-by:
To get more information on configuration details and the version of Python that Ansible is running on, run the following command:

ansible --version

This command will return something like the following output:

ansible [core 2.14.3]
  config file = None
  configured module search path = ['/home/student/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/student/.local/lib/python3.10/site-packages/ansible
  ansible collection location = /home/student/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/student/.local/bin/ansible
  python version = 3.10.6 (main, Mar 10 2023, 10:55:28) [GCC 11.3.0] (/usr/bin/python3)
  jinja version = 3.0.3
  libyaml = True
Since Ansible relies on Python being installed on the target hosts in many cases, being able to access the Python version can be quite helpful. If you incorporate Jinja2 templating or need to check that Ansible can access libyaml (a YAML parser and emitter library which dramatically increases the speed of Ansible), this second set of data can be quite helpful as well.

Mac OSX - Install Instructions
If you are a Mac user, you can use Homebrew to upgrade or install Ansible on Python 3.x, a free, open-source package manager for installing apps and software on macOS.

First, remove any version of Ansible that might already be installed:

python3 -m pip uninstall ansible

Check and see if Homebrew is installed.

brew doctor

Homebrew is known for it's clever responses, so you'll probably get something back about "potions" or "kegs in the basement". If, instead, you receive something like "Command 'brew' not found...", you'll need to install it. Start by installing Xcode:

xcode-select --install

Then, install Homebrew:

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Run brew doctor again to make sure Homebrew is installed. If it is, use Homebrew to install the latest version of Python:

brew install python3

Now, install Ansible using the following, remembering to include the "--user" to install to the local user:

python3 -m pip install --user ansible

Finally, fix the path, making sure to change out for your actual local username:

export PATH="/Users/<localuser>/Library/Python/3.9/bin:$PATH"

Run ansible --version to see the latest version of Ansible supported by Python 3.x.

Once you have Ansible up and running, consider checking out our Ansible 101: Ansible Essentials Training course to help get you up to speed with "the most powerful configuration management system on the planet!"