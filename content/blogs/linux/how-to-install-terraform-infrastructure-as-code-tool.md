---
title: "How To Install Terraform Infrastructure As Code Tool"
date: 2023-08-03
draft: false
headingPost: "Author: Joe Hutcheson"
---
With the increasing complexity of cloud infrastructure, the demand for the open-source, state management tool Terraform is on the rise. Developed by HashiCorp, Terraform gives developers an opportunity to work safely and efficiently through their various clouds using a common coding interface. More and more enterprises are shifting their workloads to cloud environments.  *Infrastructure as Code* (IaC), which is the goal of Terraform, allows these enterprises to manage and provision their environments through code, rather than a manual process.  

Getting up and running with Terraform on an Ubuntu/Debian system is *easy*. First, make sure that your system is up to date wth the following command, which downloads package information from all configured sources:
```
sudo apt-get update
```
Now, install the *GNU Privacy Guard*, *the software-properties-common*, and *the curl packages*, which will allow us to verify HashiCorp's GPG signature and install HashiCorp's Debian package repository.
```
sudo apt-get install -y gnupg software-properties-common curl
```
We need to add the HashiCorp GPG key:
```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```
The following will add the official HashiCorp Linux repository, a source of Debian and RPM packages for HashiCorp products:
```
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
```
Run the following command again to add the repository:
```
sudo apt-get update
```
And finally, this command will install the Terraform CLI:
```
sudo apt-get install terraform
```
Check to see that Terraform is installed:
```
terraform --version
```

In addition to Ubuntu/Debian, Terraform may be installed on CentOS/RHEL, Fedora, Amazon Linux, Homebrew on OS X, and Chocolatey on Windows. Full install instructions for these platforms are published by HashiCorp within the Terraform documentation pages [https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)

And there you go! You're ready to start exploring the world of *Infrastructure as Code* with Terraform. Consider doing it with [Alta3 Research's Terraform 101](https://alta3.com/overview-terraform) course!
