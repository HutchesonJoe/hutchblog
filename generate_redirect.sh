#!/bin/bash

# Specify the output file
output_file="redirects.yaml"

# Start writing the YAML header
echo "redirects:" > "$output_file"

# Loop through the list of files and append entries to the YAML file
for file in *; do
    if [ -f "$file" ]; then
        # Exclude the output file itself
        if [ "$file" != "$output_file" ]; then
            # Extract the filename without extension
            filename=$(basename "$file" .md)
            
            # Append entry to the YAML file
            echo "  - old: \"https://alta3.com/blog/$filename\"" >> "$output_file"
            echo "    new: \"$filename\"" >> "$output_file"
        fi
    fi
done

# Display completion message
echo "YAML file '$output_file' created successfully."

