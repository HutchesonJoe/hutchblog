---
title: "8 Steps To K8S Cluster"
date: 2024-02-22
draft: false
headingPost: "Author: Sam Griffith"
---

{{< youtube LfZF-xDN22w >}}

Are you looking to set up your own learning environment for Kubernetes and you're not sure where to begin?

That's not too surprising given the fact that there are over [90 different Certified Kubernetes offerings](https://www.cncf.io/certification/software-conformance/)!

That makes it _incredibly difficult_ to know where to start.

One of the new Kubernetes installation options to watch out for is called [_kind_ (<u>K</u>ubernetes <u>IN</u> <u>D</u>ocker)](https://kind.sigs.k8s.io/#why-kind). You can deploy it on Mac, Windows, or Linux, and even set up multi-node and HA clusters with it...

And the best part is that you can go from **start-to-cluster** in just 8 steps!


<img src="https://d33wubrfki0l68.cloudfront.net/d0c94836ab5b896f29728f3c4798054539303799/9f948/logo/logo.png" width="60%">


> "kind is a tool for running local Kubernetes clusters using Docker container “nodes”.
kind was primarily designed for testing Kubernetes itself, but may be used for local development or CI."" https://kind.sigs.k8s.io/

## The Setup

To follow this guide exactly, you must have the following already set up:

1. You are using an Ubuntu machine (18.04 or 20.04, virtual machine preferred)
2. You have root privileges on aforementioned machine

### Step 1

The first thing we need to do is install **docker**.

`$` `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && sudo apt-get install docker-ce -y`

### Step 2

Next we will need to download and install **kubectl**.

`$` `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl`

### Step 3

Next, we need to download and set up Golang.

`$` `wget https://golang.org/dl/go1.15.8.linux-amd64.tar.gz &&  sudo tar -C /usr/local -xzf go1.15.8.linux-amd64.tar.gz && export PATH=$PATH:/usr/local/go/bin`

### Step 4

Now that we have our system ready to **go** (do you get it?) it is time for us to start installing **kind**. Run the following **go** command to download and install kind.

`$` `GO111MODULE="on" go get sigs.k8s.io/kind@v0.10.0 && sudo su`

### Step 5

Next we need to make sure that we have our path set up correctly to use kind. **NOTE: This assumes that you have established the `go/bin` directory under the user named _ubuntu_. If you were using a different user, please use that username instead.

`#` `export PATH=$PATH:/home/ubuntu/go/bin`

> Note that this path will not stick around persistently. In order to **properly** edit this path, you would want to `sudo visudo` and append `:/home/ubuntu/go/bin` to the end of the line that says **secure_path**.

### Step 6

Now we are able to create our first **kind** cluster. The first time through, this normally takes 2 to 3 minutes as kind has to download the **kindest/node** docker image first.

`#` `kind create cluster`

```
Creating cluster "kind" ...
✓ Ensuring node image (kindest/node:v1.20.2) �
✓ Preparing nodes 📦
✓ Writing configuration �
✓ Starting control-plane �
✓ Installing CNI �
✓ Installing StorageClass �
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Have a nice day!
```

### Step 7

Trust, but verify. Make sure that your cluster was actually set up.

`#` `kind get clusters && kubectl cluster-info --context kind-kind`

```
kind
Kubernetes control plane is running at https://127.0.0.1:45547
KubeDNS is running at https://127.0.0.1:45547/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

### Step 8

Now it is time to take your cluster for a test-drive. Launch a basic nginx Pod named **mypod** in your newly created **kind** cluster and make sure that it launches and gets to a "Running" status.

`#` `kubectl run mypod --image=nginx && kubectl get pods -w`

```
pod/mypod created
NAME    READY   STATUS              RESTARTS   AGE
mypod   0/1     Pending             0          1s
mypod   0/1     ContainerCreating   0          1s
mypod   1/1     Running             0          11s
```
> Once you see your Pod with the STATUS of "Running," exit out of the `get pods -w` command by pressing **Ctrl c**.

Your brand new Kubernetes Cluster is all set up! You are now free to start learning, testing, or dev-ing on your very own cluster!



### Additional Resources
If you or your team are looking to learn more about Kubernetes, check out our [Kubernetes courses](https://alta3.com/cloud-training) and our [Kubernetes Cheat Sheet](https://static1.squarespace.com/static/5df3d8c5d2be5962e4f87890/t/5e37880a26acc86be00fac4e/1580697610429/k8s.pdf).
